﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Vector.Cloud.Logging.ControlRoom.VTT.MemoryReader.Data;

namespace Vector.Cloud.Logging.ControlRoom.VTT.MemoryReader
{

    /// <summary>
    /// RacingRoomExperience memory reader.
    /// </summary>
    public class RacingRoomExperienceMemoryReader : IDisposable
    {

        #region Properties

        private bool Mapped
        {
            get { return (_file != null); }
        }

        private Shared _data;

        private MemoryMappedFile _file;

        private byte[] _buffer;

        private TimeSpan UpdateInterval = TimeSpan.FromMilliseconds(100);

        private readonly String ExecutableName;

        private readonly List<RREMemoryDataListener> RREMemoryDataListeners = new List<RREMemoryDataListener>();

        public bool IsRunning { get; private set; }

        #endregion

        /// <summary>
        /// Construct.
        /// </summary>
        /// <param name="exeName"></param>
        public RacingRoomExperienceMemoryReader(String exeName = "RRRE")
        {
            // Properties
            this.ExecutableName = exeName;
            // State
            this.IsRunning = false;
        }

        /// <summary>
        /// Add data listener.
        /// </summary>
        /// <param name="dataListener"></param>
        public void AddDataListener(RREMemoryDataListener dataListener)
        {
            // Determine if already running
            if (this.IsRunning)
            {
                throw new Exception("VTT_RRE_MR Already running!!!");
            }
            // Validate
            if (dataListener == null)
            {
                throw new ArgumentNullException(nameof(dataListener));
            }
            // Add
            this.RREMemoryDataListeners.Add(dataListener);
        }

        /// <summary>
        /// Dispose memory mapped file.
        /// </summary>
        public void Dispose()
        {
            if (_file == null)
            {
                return;
            }
            _file.Dispose();
        }

        #region Run

        /// <summary>
        /// Determine if simulate mode is enabled.
        /// </summary>
        private bool SimulateMode = false;

        /// <summary>
        /// Run as simulation only.
        /// </summary>
        /// <returns></returns>
        public async Task Simulate()
        {
            this.SimulateMode = true;
            await this.Execute();
        }

        /// <summary>
        /// Run.
        /// </summary>
        public async Task Run()
        {
            this.SimulateMode = false;
            await this.Execute();
        }

            /// <summary>
            /// Execute.
            /// </summary>
        private async Task Execute()
        {
            // Determine if already running
            if (this.IsRunning)
            {
                throw new Exception("VTT_RRE_MR Already running!!!");
            }

            // Check if any listener present
            if (this.RREMemoryDataListeners.Count == 0)
            {
                throw new Exception("VTT_RRE_MR Register at least one RREMemoryDataListener!");
            }

            // Determine interval
            Console.WriteLine("VTT_RRE_MR Determine common interval for '{0}' listeners: ", this.RREMemoryDataListeners.Count);
            List<long> intervals = this.RREMemoryDataListeners.Select(l => l.IntervalMilliseconds).ToList();
            long updateInterval = 0;
            foreach (RREMemoryDataListener listener in this.RREMemoryDataListeners)
            {
                long interval = listener.IntervalMilliseconds;
                Console.WriteLine("           - {0}: {1} ms: ", listener.GetType().Name, interval);
                if (interval <= 0)
                {
                    throw new InvalidProgramException("RREMemoryDataListener.IntervalMilliseconds must be > 0!");
                }
                if (updateInterval == 0)
                {
                    updateInterval = interval;
                    continue;
                }
                updateInterval = this.GCDRecursive(updateInterval, interval);
            }
            this.UpdateInterval = TimeSpan.FromMilliseconds(updateInterval);
            Console.WriteLine("           --> Total: {0} ms: ", updateInterval);

            // Initialize
            Console.WriteLine("VTT_RRE_MR Initializing '{0}' listeners: ", this.RREMemoryDataListeners.Count);
            foreach (var listener in this.RREMemoryDataListeners)
            {
                Console.WriteLine("           - {0}", listener.GetType().Name);
                await listener.Init();
            }

            // Basically logic from sample project (except callbacks)
            DateTime timeReset = DateTime.UtcNow;
            DateTime timeLast = timeReset;

            // Initial state
            bool wasRunning = false;

            // Loop
            while (true)
            {

                // Timing
                DateTime timeNow = DateTime.UtcNow;
                if (timeNow.Subtract(timeLast) < UpdateInterval)
                {
                    Thread.Sleep(1);
                    continue;
                }
                timeLast = timeNow;

                // Determine if running
                bool isRunning = (this.SimulateMode) ? true : this.IsRrreRunning();
                if (isRunning && !wasRunning)
                {
                    Console.WriteLine("VTT_RRE_MR Found executable '{0}'", this.ExecutableName);
                    await this.NotifyListenersConnected();
                }
                if (wasRunning && !isRunning)
                {
                    Console.WriteLine("VTT_RRE_MR Waiting for executable '{0}'", this.ExecutableName);
                    await this.NotifyListenersDisconnected();
                }
                wasRunning = isRunning;

                // Mapping
                if (!this.SimulateMode)
                {
                    if (isRunning && !Mapped)
                    {
                        //Console.WriteLine("VTT_RRE_MR Mapping shared memory...");

                        if (Map())
                        {
                            //Console.WriteLine("VTT_RRE_MR Memory mapped successfully");

                            timeReset = DateTime.UtcNow;

                            _buffer = new Byte[Marshal.SizeOf(typeof(Shared))];
                        }
                    }
                }

                // Read from memory or simulate
                if (this.SimulateMode)
                {
                    // Simulate
                    this.SimulateRead();
                    await this.NotifyListenersData(_data);
                }
                else
                {
                    // Read from memory
                    if (Mapped)
                    {
                        if (Read())
                        {
                            await this.NotifyListenersData(_data);
                        }
                    }
                }
                
            }
        }

        #endregion

        /// <summary>
        /// Notify listeners about connection.
        /// </summary>
        /// <returns></returns>
        private async Task NotifyListenersConnected()
        {
            foreach (RREMemoryDataListener rreMemoryDataListener in this.RREMemoryDataListeners)
            {
                await rreMemoryDataListener.OnConnect();
            }
        }

        /// <summary>
        /// Notify listeners about disconnection.
        /// </summary>
        /// <returns></returns>
        private async Task NotifyListenersDisconnected()
        {
            foreach (RREMemoryDataListener rreMemoryDataListener in this.RREMemoryDataListeners)
            {
                await rreMemoryDataListener.OnDisconnect();
            }
        }

        /// <summary>
        /// Notify listeners about new data.
        /// </summary>
        /// <returns></returns>
        private async Task NotifyListenersData(Shared data)
        {
            foreach (RREMemoryDataListener rreMemoryDataListener in this.RREMemoryDataListeners)
            {
                await rreMemoryDataListener.NotifyNewData(data);
            }
        }

        private bool Map()
        {
            try
            {
                _file = MemoryMappedFile.OpenExisting(Constant.SharedMemoryName);
                return true;
            }
            catch (FileNotFoundException)
            {
                return false;
            }
        }

        /// <summary>
        /// Simulate data read.
        /// </summary>
        private void SimulateRead()
        {
            this._data = new Shared()
            {
                PlayerName = System.Text.Encoding.UTF8.GetBytes("SimulatedPlayer"),
            };
        }

        private bool Read()
        {
            try
            {
                MemoryMappedViewStream _view = _file.CreateViewStream();
                BinaryReader _stream = new BinaryReader(_view);
                _buffer = _stream.ReadBytes(Marshal.SizeOf(typeof(Shared)));
                GCHandle _handle = GCHandle.Alloc(_buffer, GCHandleType.Pinned);
                _data = (Shared)Marshal.PtrToStructure(_handle.AddrOfPinnedObject(), typeof(Shared));
                _handle.Free();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Utility

        public long GCDRecursive(long a, long b)
        {
            if (a == 0)
                return b;
            if (b == 0)
                return a;

            if (a > b)
                return GCDRecursive(a % b, b);
            else
                return GCDRecursive(a, b % a);
        }

        public bool IsRrreRunning()
        {
            return Process.GetProcessesByName(this.ExecutableName).Length > 0;
        }

        #endregion

    }
}
