﻿using System;
using System.Threading.Tasks;
using Vector.Cloud.Logging.ControlRoom.VTT.MemoryReader.Data;

namespace Vector.Cloud.Logging.ControlRoom.VTT.MemoryReader
{

    /// <summary>
    /// Memory data listener for handing of new incoming data.
    /// </summary>
    public abstract class RREMemoryDataListener
    {

        #region Abstract

        /// <summary>
        /// Interval in milliseconds.
        /// </summary>
        public abstract long IntervalMilliseconds { get; }

        /// <summary>
        /// Determine if HandleData is called between intervals when new data available.
        /// </summary>
        public abstract bool HandleDataBetweenIntervals { get; }

        /// <summary>
        /// Initialize
        /// </summary>
        public abstract Task Init();

        /// <summary>
        /// On connecting to RRRE.
        /// </summary>
        /// <returns></returns>
        public virtual async Task OnConnect()
        {
            await Task.CompletedTask;
        }

        /// <summary>
        /// Handle data (async).
        /// </summary>
        /// <param name="data"></param>
        /// <param name="onInterval"></param>
        /// <returns></returns>
        protected abstract Task HandleData(Shared data, bool onInterval);

        /// <summary>
        /// On disconnecting from RRRE.
        /// </summary>
        /// <returns></returns>
        public virtual async Task OnDisconnect()
        {
            await Task.CompletedTask;
        }

        #endregion

        #region HandleNewDataNotification

        /// <summary>
        /// Store last execution.
        /// </summary>
        protected DateTime LastExecution = DateTime.MinValue;

        /// <summary>
        /// Notify new data available.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task NotifyNewData(Shared data)
        {
            DateTime now = DateTime.Now;
            bool onInterval = (this.LastExecution.Add(TimeSpan.FromMilliseconds(this.IntervalMilliseconds)) <= now);
            if (onInterval)
            {
                this.LastExecution = now;
            }
            if (onInterval || this.HandleDataBetweenIntervals)
            {
                await this.HandleData(data, onInterval);
            }
        }

        #endregion

    }
}