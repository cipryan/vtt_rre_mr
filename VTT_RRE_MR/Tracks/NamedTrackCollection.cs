﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Vector.Cloud.Logging.ControlRoom.VTT.Tracks
{

    /// <summary>
    /// Collection of named tracks.
    /// </summary>
    public class NamedTrackCollection : List<NamedTrack>
    {

        #region Static/Create

        /// <summary>
        /// Create by all NamedTrack types in this assembly.
        /// </summary>
        /// <returns></returns>
        public static NamedTrackCollection ByReflection()
        {
            return ByReflection(Assembly.GetAssembly(typeof(Startup)));
        }

        /// <summary>
        /// Create by all NamedTrack types in specified assembly.
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static NamedTrackCollection ByReflection(Assembly assembly)
        {
            NamedTrackCollection c = new NamedTrackCollection();
            List<Type> types = assembly.GetTypes()
                .Where(t => typeof(NamedTrack).IsAssignableFrom(t))
                .Where(t => !t.IsAbstract && !t.IsGenericType)
                .ToList();
            types.ForEach(t => {
                NamedTrack nt = (NamedTrack)Activator.CreateInstance(t);
                c.Add(nt);
            });
            return c;
        }

        #endregion

        #region Getters

        /// <summary>
        /// Check if has track (by name).
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool HasTrack(String name)
        {
            return this.Where(nt => nt.Name.Equals(name)).Any();
        }

        /// <summary>
        /// Get track by name (or null if not exists).
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public NamedTrack GetTrackByName(String name)
        {
            return this.Where(nt => nt.Name.Equals(name)).FirstOrDefault();
        }

        #endregion

    }
}
