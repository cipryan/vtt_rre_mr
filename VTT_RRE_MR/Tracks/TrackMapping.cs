﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Linq;

namespace Vector.Cloud.Logging.ControlRoom.VTT.Tracks
{
    public class TrackMapping
    {

        /// <summary>
        /// 2D transformation matrix.
        /// </summary>
        public readonly Matrix<double> TransformationMatrix;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="alpha"></param>
        /// <param name="beta"></param>
        /// <param name="delta"></param>
        /// <param name="gamma"></param>
        /// <param name="gamma"></param>
        /// <param name="zetva"></param>
        public TrackMapping(double alpha, double beta, double gamma, double delta, double epsilon, double zeta)
        {
            this.TransformationMatrix = Matrix<double>.Build.DenseOfArray(new double[,] {
                { alpha, beta, gamma },
                { delta, epsilon, zeta },
            });
        }

        public static TrackMapping FromCoordinateMappings(params CoordinateMapping[] cms)
        {
            if (cms.Length < 3)
            {
                throw new ArgumentException("Require at least 3 CoordinateMapping items to create transition matrix!");
            }
            Matrix<double> A = Matrix<double>.Build.DenseOfRowArrays(cms.Select(cm => new double[] { cm.X, cm.Y, 1 }));
            // alpha & beta
            Vector<double> xVector = Vector<double>.Build.Dense(cms.Select(cm => cm.Lat).ToArray());
            Vector<double> x = A.Solve(xVector);
            double alpha = x[0];
            double beta = x[1];
            double gamma = x[2];
            // gamma & delta
            Vector<double> yVector = Vector<double>.Build.Dense(cms.Select(cm => cm.Lon).ToArray());
            Vector<double> y = A.Solve(yVector);
            double delta = y[0];
            double epsilon = y[1];
            double zeta = y[2];
            // return
            return new TrackMapping(alpha, beta, gamma, delta, epsilon, zeta);
        }

        public Vector<double> GetGps(double x, double y)
        {
            Vector<double> m = Vector<double>.Build.Dense(new double[] { x, y, 1 });
            return this.TransformationMatrix.Multiply(m);
        }
    }
}
