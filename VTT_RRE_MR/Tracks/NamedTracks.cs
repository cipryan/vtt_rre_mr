﻿using System;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;

namespace Vector.Cloud.Logging.ControlRoom.VTT.Tracks
{

    /// <summary>
    /// Represents a named track.
    /// </summary>
    public abstract class NamedTrack
    {

        #region Abstract

        /// <summary>
        /// Track name.
        /// </summary>
        public abstract String Name { get; }

        /// <summary>
        /// List of known coordinate mappings.
        /// </summary>
        public abstract List<CoordinateMapping> CoordinateMappings { get; }

        /// <summary>
        /// Fix for ingame degree.
        /// </summary>
        public abstract double CourseFixDegree { get; }

        #endregion

        #region Properties

        /// <summary>
        /// Track mapping instance.
        /// </summary>
        protected TrackMapping TrackMapping { get; }

        #endregion

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public NamedTrack()
        {
            this.TrackMapping = TrackMapping.FromCoordinateMappings(this.CoordinateMappings.ToArray());
        }

        #region GPS

        /// <summary>
        /// Get GPS coordinates from ingame map coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Vector<double> GetGps(double x, double y)
        {
            return this.TrackMapping.GetGps(x, y);
        }

        #endregion

    }
}
