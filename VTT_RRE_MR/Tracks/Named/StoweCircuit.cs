﻿using System.Collections.Generic;

namespace Vector.Cloud.Logging.ControlRoom.VTT.Tracks.Named
{
    public class StoweCircuit : NamedTrack
    {

        public override string Name => "Stowe Circuit";

        public override double CourseFixDegree => -175.0d;

        public override List<CoordinateMapping> CoordinateMappings => new List<CoordinateMapping>()
        {
            new CoordinateMapping(-182.3829, -3.533724, 52.066592, -1.019502),
            new CoordinateMapping(208.6068, 5.120338, 52.071157, -1.013779),
            new CoordinateMapping(-284.7001, -3.958500, 52.067796, -1.021128),
            new CoordinateMapping(122.09, 1.904074, 52.069408, -1.015049),
            new CoordinateMapping(26.5528, -0.45351, 52.067971, -1.016277)
        };

    }
}
