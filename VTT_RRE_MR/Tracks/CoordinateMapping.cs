﻿using System.Numerics;

namespace Vector.Cloud.Logging.ControlRoom.VTT.Tracks
{
    public class CoordinateMapping
    {

        public readonly double X;
        public readonly double Y;
        public readonly double Lat;
        public readonly double Lon;

        public CoordinateMapping(double x, double y, double lat, double lon)
        {
            this.X = x;
            this.Y = y;
            this.Lat = lat;
            this.Lon = lon;
        }
    }
}