﻿using System;

namespace Vector.Cloud.Logging.ControlRoom.VTT.LoggerDevices
{

    /// <summary>
    /// GL Logger representation.
    /// </summary>
    public class GlLoggerDevice
    {

        /// <summary>
        /// Logger serial.
        /// </summary>
        public readonly String Serial;

        /// <summary>
        /// Construct by serial.
        /// </summary>
        /// <param name="serial"></param>
        public GlLoggerDevice(string serial)
        {
            Serial = serial;
        }
    }
}
