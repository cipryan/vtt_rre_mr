﻿using System.Threading.Tasks;
using Vector.Cloud.Logging.ControlRoom.VTT.MemoryReader;
using Vector.Cloud.Logging.ControlRoom.VTT.MemoryReader.Data;
using Vector.Cloud.Logging.ControlRoom.VTT.Tracks;

namespace Vector.Cloud.Logging.ControlRoom.VTT.DataListeners
{

    /// <summary>
    /// Extended data listener, knowing about tracks and loggers.
    /// </summary>
    public abstract class ExtendedDataListener : RREMemoryDataListener
    {

        /// <summary>
        /// Collection of known named tracks.
        /// </summary>
        public readonly NamedTrackCollection NamedTrackCollection;

        /// <summary>
        /// Construct by named track collection.
        /// </summary>
        /// <param name="namedTrackCollection"></param>
        public ExtendedDataListener(NamedTrackCollection namedTrackCollection)
        {
            this.NamedTrackCollection = namedTrackCollection;
        }

        /// <summary>
        /// Handle data by generating extended memory data.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="onInterval"></param>
        /// <returns></returns>
        protected override async Task HandleData(Shared data, bool onInterval)
        {
            await this.HandleData(ExtendedMemoryData.Create(data, this.NamedTrackCollection), onInterval);
        }

        /// <summary>
        /// Handle.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="onInterval"></param>
        /// <returns></returns>
        protected abstract Task HandleData(ExtendedMemoryData data, bool onInterval);
    }
}
