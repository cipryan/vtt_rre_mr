﻿using System;
using System.Threading.Tasks;
using Vector.Cloud.Logging.ControlRoom.VTT.Tracks;

namespace Vector.Cloud.Logging.ControlRoom.VTT.DataListeners.Logging
{

    /// <summary>
    /// Memory listener printing to console.
    /// </summary>
    public class PrintToConsoleListener : ExtendedDataListener
    {

        #region Properties

        /// <summary>
        /// Execute once every second.
        /// </summary>
        public override long IntervalMilliseconds => 1000;

        /// <summary>
        /// Even show data in between.
        /// </summary>
        public override bool HandleDataBetweenIntervals => true;

        #endregion

        #region Create/Init

        /// <summary>
        /// Construct with named track collection.
        /// </summary>
        /// <param name="namedTrackCollection"></param>
        public PrintToConsoleListener(NamedTrackCollection namedTrackCollection) : base(namedTrackCollection) { }

        /// <summary>
        /// Initialize.
        /// </summary>
        /// <returns></returns>
        public override async Task Init()
        {
            await Task.CompletedTask;
        }

        #endregion

        #region Handle

        /// <summary>
        /// Just print to console that new data is available.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="onInterval"></param>
        /// <returns></returns>
        protected override async Task HandleData(ExtendedMemoryData data, bool onInterval)
        {
            this.Print(data);
            await Task.CompletedTask;
        }

        /// <summary>
        /// Simple print.
        /// </summary>
        /// <param name="data"></param>
        private void Print(ExtendedMemoryData data)
        {
            /*
            var stream1 = new MemoryStream();
            var ser = new DataContractJsonSerializer(typeof(Shared));
            ser.WriteObject(stream1, data);
            stream1.Position = 0;
            var sr = new StreamReader(stream1);
            var str = sr.ReadToEnd();
            Console.WriteLine(str);
            */

            if (data.IsActive)
            {
                Console.WriteLine("");
                Console.WriteLine("Name: {0}, Track: {1}", data.PlayerName, data.TrackName);
                if (data.HasGps)
                {
                    Console.WriteLine("Position: ({0} / {1}), Course: {2}, Speed: {3}",
                        data.Gps.Latitude, data.Gps.Longitude, data.Gps.Course, data.Gps.Speed);
                }

            }
        }

        #endregion

        #region Utility

        public static Single RpsToRpm(Single rps)
        {
            return rps * (60 / (2 * (Single)Math.PI));
        }

        public static Single MpsToKph(Single mps)
        {
            return mps * 3.6f;
        }

        #endregion
    }
}
