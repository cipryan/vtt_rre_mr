﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Device.Location;
using System.Text;
using Vector.Cloud.Logging.ControlRoom.VTT.MemoryReader.Data;
using Vector.Cloud.Logging.ControlRoom.VTT.Tracks;

namespace Vector.Cloud.Logging.ControlRoom.VTT.DataListeners
{

    /// <summary>
    /// Extended memory data.
    /// </summary>
    public class ExtendedMemoryData
    {

        #region Properties

        /// <summary>
        /// Original shared memory data object.
        /// </summary>
        public readonly Shared Shared;

        /// <summary>
        /// Determine if active (on a map).
        /// </summary>
        public readonly bool IsActive;

        /// <summary>
        /// Track name (may be empty).
        /// </summary>
        public readonly string TrackName;

        /// <summary>
        /// Determine if has track name.
        /// </summary>
        public bool HasTrackName => !String.IsNullOrEmpty(this.TrackName);

        /// <summary>
        /// Gps data (may be null).
        /// </summary>
        public readonly GeoCoordinate Gps;

        /// <summary>
        /// Determine if has gps data.
        /// </summary>
        public bool HasGps => this.Gps != null;

        /// <summary>
        /// Determine if has player name.
        /// </summary>
        public readonly string PlayerName;

        /// <summary>
        /// Determine if has player name.
        /// </summary>
        public bool HasPlayerName => !String.IsNullOrEmpty(this.PlayerName);

        #endregion

        #region Create

        /// <summary>
        /// Construct.
        /// </summary>
        /// <param name="shared"></param>
        /// <param name="isActive"></param>
        /// <param name="trackName"></param>
        /// <param name="gps"></param>
        /// <param name="playerName"></param>
        public ExtendedMemoryData(Shared shared, bool isActive, string trackName, GeoCoordinate gps, string playerName)
        {
            this.Shared = shared;
            this.IsActive = isActive;
            this.TrackName = trackName;
            this.Gps = gps;
            this.PlayerName = playerName;
        }

        /// <summary>
        /// Create by reading info in shared.
        /// </summary>
        /// <param name="shared"></param>
        /// <param name="namedTrackCollection"></param>
        /// <returns></returns>
        public static ExtendedMemoryData Create(Shared shared, NamedTrackCollection namedTrackCollection)
        {
            // Track
            String trackName = (Encoding.UTF8.GetString(shared.TrackName) ?? String.Empty).TrimEnd('\0');
            // Online
            bool isActive = !String.IsNullOrEmpty(trackName)
                            && (shared.GameInMenus == 0)
                            && (shared.GamePaused == 0)
                            && (shared.GameInReplay == 0);
            // GPS
            GeoCoordinate gps = null;
            if (namedTrackCollection.HasTrack(trackName))
            {
                var track = namedTrackCollection.GetTrackByName(trackName);
                Vector<double> position = track.GetGps(shared.CarCgLocation.X, shared.CarCgLocation.Y);
                double yaw_degrees = (shared.CarOrientation.Yaw * 180.0d / Math.PI) + track.CourseFixDegree;
                double course = Math.Round(PositiveMod(yaw_degrees, 360.0d), 1);
                double speed = Math.Round(((shared.CarSpeed < 0) ? 0.0d : (double)shared.CarSpeed), 1);
                gps = new GeoCoordinate(position[0], position[1])
                {
                    Course = course,
                    Speed = speed
                };
            }
            // Player
            String playerName = (Encoding.UTF8.GetString(shared.PlayerName) ?? String.Empty).TrimEnd('\0');
            // Create
            return new ExtendedMemoryData(shared, isActive, trackName, gps, playerName);
        }

        #endregion

        #region Utility

        /// <summary>
        /// Modulo (positive).
        /// </summary>
        /// <param name="x"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        private static double PositiveMod(double x, double m)
        {
            return (x % m + m) % m;
        }

        #endregion

    }
}