﻿using MQTTnet.Client.Options;
using System;
using System.Threading.Tasks;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Configuration;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Logging;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish;
using Vector.Cloud.Logging.ControlRoom.VTT.LoggerDevices;
using Vector.Cloud.Logging.ControlRoom.VTT.Tracks;
using Vector.Cloud.Logging.LiveDataChannel.Root.Messages.Data;
using Vector.Cloud.Logging.LiveDataChannel.Root.Messages.Logger;

namespace Vector.Cloud.Logging.ControlRoom.VTT.DataListeners.LiveDataChannel
{

    /// <summary>
    /// Memory listener sending data to Live-Data-Channel.
    /// </summary>
    public class SendToLiveDataChannelListener : ExtendedDataListener
    {
        /// <summary>
        /// Construct by server address.
        /// </summary>
        /// <param name="serverAddress"></param>
        public SendToLiveDataChannelListener(String serverAddress, NamedTrackCollection namedTrackCollection, GlLoggerDevice loggerDevice)
            : base(namedTrackCollection)
        {
            this.ServerAddress = serverAddress ?? throw new ArgumentNullException(nameof(serverAddress));
            this.LoggerDevice = loggerDevice;
            this.IsOnline = false;
            this.LastSentOnline = false;
            this.LastSendSuccess = false;
            this.LastSentLogging = false;
        }

        #region Logging-Device

        /// <summary>
        /// GL logger device.
        /// </summary>
        public readonly GlLoggerDevice LoggerDevice;

        /// <summary>
        /// Determines if Logging-Device is online.
        /// </summary>
        public bool IsOnline { get; protected set; }
        #endregion

        #region RREMemoryDataListener

        /// <summary>
        /// Receive data every 5 sec.
        /// </summary>
        public override long IntervalMilliseconds => 5000;

        /// <summary>
        /// Do handle data between intervals if last interval send failed.
        /// </summary>
        public override bool HandleDataBetweenIntervals => true;

        /// <summary>
        /// Initialize MQTT
        /// </summary>
        /// <returns></returns>
        public override async Task Init()
        {
            // MQTT options
            IMqttClientOptions mqttClientOptions = new MqttClientOptionsBuilder()
                .WithTcpServer(this.ServerAddress, 1883)
                .Build();
            // Mqtt-Serializable data manager.
            this.LiveDataManager = LiveDataManager.Builder
               .WithMqttClientOptions(mqttClientOptions)
               .WithoutStorageRepository()
               .WithReflectedSubscriptionMessageHandlers()
               .WithConfiguration(new LiveDataManagerConfiguration(5, 5))
               .WithLogger(ConsoleLogger.Create())
               .Build();
            // Init
            await this.LiveDataManager.InitAsync();
        }

        /// <summary>
        /// Change logger state when connecting.
        /// </summary>
        /// <returns></returns>
        public override async Task OnConnect()
        {
            this.IsOnline = true;
            await this.SendOnline();
        }

        /// <summary>
        /// Change logger state when disconnecting.
        /// </summary>
        /// <returns></returns>
        public override async Task OnDisconnect()
        {
            this.IsOnline = false;
            await this.SendOffline();
        }

        /// <summary>
        /// Handle data.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="onInterval"></param>
        /// <returns></returns>
        protected override async Task HandleData(ExtendedMemoryData data, bool onInterval)
        {
            // Only send messages if correctly notified about online state
            if (this.IsOnline != this.LastSentOnline)
            {
                return;
            }
            // Logging state
            await this.HandleLoggingState(data.IsActive);
            // Pulse
            await this.HandlePulse(data, onInterval);
        }

        #endregion

        #region LiveDataChannel

        /// <summary>
        /// Mqtt-Serializable data manager.
        /// </summary>
        protected LiveDataManager LiveDataManager;

        /// <summary>
        /// Server address.
        /// </summary>
        protected readonly String ServerAddress;

        /// <summary>
        /// Determine if last send was a success.
        /// </summary>
        protected bool LastSendSuccess = false;

        /// <summary>
        /// Determines last (successfully) sent logging state of Logging-Device.
        /// </summary>
        public bool LastSentLogging { get; protected set; }

        /// <summary>
        /// Determines last (successfully) sent online state of Logging-Device.
        /// </summary>
        public bool LastSentOnline { get; protected set; }

        /// <summary>
        /// Last valid GPS info.
        /// </summary>
        public Gps LastGps { get; protected set; }

        /// <summary>
        /// Try to send message but throw exception on failure.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected async Task TrySendMessage(LiveDataPublishMessage message)
        {
            if (!this.LiveDataManager.IsConnected)
            {
                throw new Exception();
            }
            await this.LiveDataManager.PublishMessageAsync(message);
        }

        /// <summary>
        /// Send online message
        /// </summary>
        /// <returns></returns>
        protected async Task SendOnline()
        {
            while (this.IsOnline)
            {
                try
                {
                    ConnectMessage msg = new ConnectMessage(this.LoggerDevice.Serial, "startup");
                    await this.TrySendMessage(msg);
                    this.LastSentOnline = true;
                    break;
                }
                catch (Exception)
                {
                    // Retry after short delay
                    await Task.Delay(500);
                }
            }
        }

        /// <summary>
        /// Send offline message
        /// </summary>
        /// <returns></returns>
        protected async Task SendOffline()
        {
            while (!this.IsOnline)
            {
                try
                {
                    DisconnectMessage msg = new DisconnectMessage(this.LoggerDevice.Serial, "shutdown");
                    await this.TrySendMessage(msg);
                    this.LastSentOnline = false;
                    break;
                }
                catch (Exception)
                {
                    // Retry after short delay
                    await Task.Delay(500);
                }
            }
        }

        /// <summary>
        /// Promote logging state change.
        /// </summary>
        /// <param name="isLogging"></param>
        /// <returns></returns>
        protected async Task HandleLoggingState(bool isLogging)
        {
            if (this.LastSentLogging != isLogging)
            {
                try
                {
                    String key = (isLogging) ? "logging-started" : "logging-stopped";
                    var msg = new KvpEventMessage(this.LoggerDevice.Serial, "logging-started", "trigger:RRRE");
                    await this.TrySendMessage(msg);
                    this.LastSentLogging = isLogging;
                }
                catch (Exception)
                {
                    // Retry next time
                }
            }
        }

        /// <summary>
        /// Send pulse.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected async Task HandlePulse(ExtendedMemoryData data, bool onInterval)
        {
            // Check if sendind data is required
            if (!onInterval && this.LastSendSuccess)
            {
                return;
            }
            // Send pulse
            try
            {
                // GPS info
                Gps gps;
                if (data.HasGps)
                {
                    gps = new Gps(data.Gps.Latitude, data.Gps.Longitude, data.Gps.Altitude, data.Gps.Speed, data.Gps.Course);
                }
                else if (this.LastGps != null)
                {
                    gps = this.LastGps;
                }
                else
                {
                    gps = new Gps(0, 0, 0, 0, 0);
                }
                this.LastGps = gps;
                // Pulse message
                PulseMessage pulse = new PulseMessage(this.LoggerDevice.Serial, gps);
                await this.TrySendMessage(pulse);
                // Success
                this.LastSendSuccess = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("MQTT Exception: {0}", e.Message);
                this.LastSendSuccess = false;
            }
        }

        #endregion
    }
}
