﻿using System;
using Vector.Cloud.Logging.ControlRoom.VTT.DataListeners.LiveDataChannel;
using Vector.Cloud.Logging.ControlRoom.VTT.DataListeners.Logging;
using Vector.Cloud.Logging.ControlRoom.VTT.LoggerDevices;
using Vector.Cloud.Logging.ControlRoom.VTT.MemoryReader;
using Vector.Cloud.Logging.ControlRoom.VTT.Tracks;

namespace Vector.Cloud.Logging.ControlRoom.VTT
{

    /// <summary>
    /// Startup.
    /// </summary>
    public class Startup
    {

        /// <summary>
        /// Register listeners and run.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("");
            Console.WriteLine("   --------- VTT_RRE_MR ---------   ");
            Console.WriteLine("");
            // Logger
            GlLoggerDevice logger = new GlLoggerDevice("123456-123456");
            // Tracks
            NamedTrackCollection ntc = NamedTrackCollection.ByReflection();
            Console.WriteLine("VTT_RRE_MR Knows tracks (found by reflection): {0} ", ntc.Count);
            foreach (NamedTrack nt in ntc)
            {
                Console.WriteLine("           - {0}", nt.Name);
            }
            using (RacingRoomExperienceMemoryReader rremr = new RacingRoomExperienceMemoryReader("RRRE64"))
            {
                // Config
                rremr.AddDataListener(new PrintToConsoleListener(ntc));
                rremr.AddDataListener(new SendToLiveDataChannelListener("localhost", ntc, logger));
                // Run (sync)
                rremr.Run().Wait();
            }

        }
    }
}
