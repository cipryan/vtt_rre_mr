﻿using MQTTnet;
using System.IO;
using System.Runtime.Serialization.Json;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Subscribe
{

    /// <summary>
    /// Live data json subscription message handler. Will be instantiated by reflection in LiveDataManager constructor.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class LiveDataSubscriptionHandlerJson<T> : LiveDataSubscriptionHandlerJson<T, object>
        where T : LiveDataPublishMessageJson
    { }

    /// <summary>
    /// Live data json subscription message handler. Will be instantiated by reflection in LiveDataManager constructor.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class LiveDataSubscriptionHandlerJson<T, TStorageRepository> : LiveDataSubscriptionHandler<T, TStorageRepository>
        where T : LiveDataPublishMessageJson where TStorageRepository : class
    {

        #region LiveDataSubscriptionHandler

        /// <summary>
        /// Deserialize typed payload. Throws Exception on failure.
        /// </summary>
        /// <param name="applicationMessage"></param>
        /// <returns></returns>
        protected override T GetTypedPayload(MqttApplicationMessage applicationMessage)
        {
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream(applicationMessage.Payload))
            {
                return (T)jsonSerializer.ReadObject(ms);
            }
        }

        #endregion

    }

}
