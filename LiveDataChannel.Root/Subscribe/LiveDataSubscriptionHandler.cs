﻿using MQTTnet;
using MQTTnet.Protocol;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Logging;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Subscribe
{

    /// <summary>
    /// Live data subscription message handler.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class LiveDataSubscriptionHandler<T> : LiveDataSubscriptionHandler<T, object>, ILiveDataInfo, ILiveDataSubscriptionHandler
    {

        #region Content

        /// <summary>
        /// Handle.
        /// </summary>
        /// <param name="liveDataManager"></param>
        /// <param name="applicationMessage"></param>
        /// <param name="logger"></param>
        /// <param name="storageRepository"></param>
        /// <param name="payload"></param>
        /// <returns>Determine if message handling should be continued</returns>
        protected abstract Task<bool> HandleAsync(LiveDataManager<object> liveDataManager, MqttApplicationMessage applicationMessage,
            ILogger logger, T payload);

        /// <summary>
        /// Handle.
        /// </summary>
        /// <param name="liveDataManager"></param>
        /// <param name="applicationMessage"></param>
        /// <param name="logger"></param>
        /// <param name="storageRepository"></param>
        /// <param name="payload"></param>
        /// <returns></returns>
        protected override async Task<bool> HandleAsync(LiveDataManager<object> liveDataManager, MqttApplicationMessage applicationMessage, 
            ILogger logger, object storageRepository, T payload)
        {
            return await this.HandleAsync(liveDataManager, applicationMessage, logger, payload);
        }

        #endregion

    }

    /// <summary>
    /// Live data subscription message handler.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class LiveDataSubscriptionHandler<T, TStorageRepository> : ILiveDataInfo, ILiveDataSubscriptionHandler<TStorageRepository> where TStorageRepository : class
    {

        #region ILiveDataSubscriptionInfo

        /// <summary>
        /// Topic.
        /// </summary>
        public abstract string Topic { get; }

        /// <summary>
        /// Generated Regex from Topic string.
        /// </summary>
        private Regex topicRegex = null;

        /// <summary>
        /// Quality of service level between broker and this (server) client.
        /// </summary>
        public virtual MqttQualityOfServiceLevel MqttQualityOfServiceLevel => MqttQualityOfServiceLevel.AtMostOnce;

        /// <summary>
        /// Topic pattern.
        /// </summary>
        public Regex TopicRegex
        {
            get
            {
                if (this.topicRegex == null)
                {
                    string regexString = string.Format("^{0}", this.Topic.Replace("+", @"([^/\n]*)").Replace("#", @".+"));
                    this.topicRegex = new Regex(regexString);
                }
                return this.topicRegex;
            }
        }

        /// <summary>
        /// Determine if topic pattern matches.
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public bool TopicPatternMatches(string topic)
        {
            return this.TopicRegex.IsMatch(topic);
        }

        #endregion

        #region Content

        /// <summary>
        /// Handle.
        /// </summary>
        /// <param name="liveDataManager"></param>
        /// <param name="applicationMessage"></param>
        /// <param name="logger"></param>
        /// <param name="storageRepository"></param>
        /// <param name="payload"></param>
        /// <returns>Determine if message handling should be continued</returns>
        protected abstract Task<bool> HandleAsync(LiveDataManager<TStorageRepository> liveDataManager, MqttApplicationMessage applicationMessage,
            ILogger logger, TStorageRepository storageRepository, T payload);

        /// <summary>
        /// Handle MQTT application message.
        /// </summary>
        /// <param name="liveDataManager"></param>
        /// <param name="applicationMessage"></param>
        /// <param name="logger"></param>
        /// <param name="storageRepository"></param>
        /// <returns>Determine if message handling should be continued</returns>
        public async Task<bool> HandleApplicationMessage(LiveDataManager<TStorageRepository> liveDataManager, MqttApplicationMessage applicationMessage, ILogger logger,
            TStorageRepository storageRepository)
        {
            T payload = this.GetTypedPayload(applicationMessage);
            return await this.HandleAsync(liveDataManager, applicationMessage, logger, storageRepository, payload);
        }

        /// <summary>
        /// Get typed payload.
        /// </summary>
        /// <param name="applicationMessage"></param>
        /// <returns></returns>
        protected abstract T GetTypedPayload(MqttApplicationMessage applicationMessage);

        #endregion

    }

}
