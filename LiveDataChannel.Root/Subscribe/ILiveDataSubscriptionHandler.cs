﻿using MQTTnet;
using System.Threading.Tasks;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Logging;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Subscribe
{

    /// <summary>
    /// Provides methods to handle subscribed messages.
    /// </summary>
    public interface ILiveDataSubscriptionHandler : ILiveDataSubscriptionHandler<object>, ILiveDataInfo
    {

    }

    /// <summary>
    /// Provides methods to handle subscribed messages.
    /// </summary>
    public interface ILiveDataSubscriptionHandler<TStorageRepository> : ILiveDataInfo where TStorageRepository : class
    {

        /// <summary>
        /// Handle MQTT application message.
        /// </summary>
        /// <param name="liveDataManager"></param>
        /// <param name="applicationMessage"></param>
        /// <param name="logger"></param>
        /// <param name="storageRepository"></param>
        /// <returns>Determine if message handling should be continued</returns>
        Task<bool> HandleApplicationMessage(LiveDataManager<TStorageRepository> liveDataManager, MqttApplicationMessage applicationMessage,
            ILogger logger, TStorageRepository storageRepository);

        /// <summary>
        /// Determine if topic pattern matches.
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        bool TopicPatternMatches(string topic);
    }
}