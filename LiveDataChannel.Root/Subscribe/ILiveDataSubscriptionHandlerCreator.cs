﻿namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Subscribe
{

    /// <summary>
    /// Handle creator.
    /// </summary>
    /// <typeparam name="TStorageRepository"></typeparam>
    public interface ILiveDataSubscriptionHandlerCreator<TStorageRepository> where TStorageRepository : class
    {

        /// <summary>
        /// Create handler.
        /// </summary>
        /// <returns></returns>
        ILiveDataSubscriptionHandler<TStorageRepository> GetHandler();

    }
}
