﻿using System.Runtime.Serialization;

namespace Vector.Cloud.Logging.LiveDataChannel.Root.Messages.Data
{

    /// <summary>
    /// Serializable GPS info.
    /// </summary>
    [DataContract]
    public class Gps
    {

        #region Properties

        [DataMember(Name = "latitude")]
        public double Latitude { get; protected set; }

        [DataMember(Name = "longitude")]
        public double Longitude { get; protected set; }

        [DataMember(Name = "altitude")]
        public double Altitude { get; protected set; }

        [DataMember(Name = "speed_ms")]
        public double SpeedMs { get; protected set; }

        [DataMember(Name = "course_degree")]
        public double CourseDegree { get; protected set; }

        #endregion

        #region Create

        /// <summary>
        /// Default constructor for serialization.
        /// </summary>
        protected Gps() : base() { }

        /// <summary>
        /// Construct by values.
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="altitude"></param>
        /// <param name="speedMs"></param>
        /// <param name="courseDegree"></param>
        public Gps(double latitude, double longitude, double altitude, double speedMs, double courseDegree) : this()
        {
            Latitude = latitude;
            Longitude = longitude;
            Altitude = altitude;
            SpeedMs = speedMs;
            CourseDegree = courseDegree;
        }

        #endregion

    }
}
