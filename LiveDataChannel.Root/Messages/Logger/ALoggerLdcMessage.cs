﻿using System;
using System.Runtime.Serialization;

namespace Vector.Cloud.Logging.LiveDataChannel.Root.Messages.Logger
{

    /// <summary>
    /// Live-Data-Channel message sent by logger.
    /// </summary>
    [DataContract]
    public abstract class ALoggerLdcMessage : LdcMessage
    {

        #region Publish

        /// <summary>
        /// Role type identifier.
        /// </summary>
        [IgnoreDataMember]
        public override string RoleTypeIdentifier => "logger";

        #endregion

        #region Create

        /// <summary>
        /// Default constructor for serialization.
        /// </summary>
        protected ALoggerLdcMessage() : base() { }

        /// <summary>
        /// Construct with identifier.
        /// </summary>
        /// <param name="identifier"></param>
        public ALoggerLdcMessage(String identifier) : base(identifier) { }

        #endregion
    }
}
