﻿using System;
using System.Runtime.Serialization;

namespace Vector.Cloud.Logging.LiveDataChannel.Root.Messages.Logger
{

    /// <summary>
    /// Live-Data-Channel KVP-Event message sent by logger.
    /// </summary>
    [DataContract]
    public class KvpEventMessage : ALoggerLdcMessage
    {

        #region Publish

        /// <summary>
        /// Message type identifier.
        /// </summary>
        [IgnoreDataMember]
        public override string MessageTypeIdentifier => "kvp-event";

        #endregion

        #region Content

        [DataMember(Name = "key")]
        public string Key { get; protected set; }

        [DataMember(Name = "value")]
        public string Value { get; protected set; }

        #endregion

        #region Create

        /// <summary>
        /// Default constructor for serialization.
        /// </summary>
        protected KvpEventMessage() : base() { }

        /// <summary>
        /// Construct by kvp.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public KvpEventMessage(String identifier, String key, String value) : base(identifier)
        {
            this.Key = key;
            this.Value = value;
        }

        #endregion

    }
}
