﻿using System;
using System.Runtime.Serialization;

namespace Vector.Cloud.Logging.LiveDataChannel.Root.Messages.Logger
{

    /// <summary>
    /// Live-Data-Channel connect message sent by logger.
    /// </summary>
    [DataContract]
    public class ConnectMessage : ALoggerLdcMessage
    {

        #region Publish

        /// <summary>
        /// Message type identifier.
        /// </summary>
        [IgnoreDataMember]
        public override string MessageTypeIdentifier => "connect";

        #endregion

        #region Content

        [DataMember(Name = "reason")]
        public string Reason { get; private set; }

        #endregion

        #region Create

        /// <summary>
        /// Default constructor for serialization.
        /// </summary>
        protected ConnectMessage() : base() { }

        /// <summary>
        /// Construct by reason.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="reason"></param>
        public ConnectMessage(String identifier, String reason) : base(identifier)
        {
            this.Reason = reason;
        }

        #endregion

    }
}
