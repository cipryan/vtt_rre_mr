﻿using System;
using System.Runtime.Serialization;

namespace Vector.Cloud.Logging.LiveDataChannel.Root.Messages.Logger
{

    /// <summary>
    /// Live-Data-Channel disconnect message sent by logger.
    /// </summary>
    [DataContract]
    public class DisconnectMessage : ALoggerLdcMessage
    {

        #region Publish

        /// <summary>
        /// Message type identifier.
        /// </summary>
        [IgnoreDataMember]
        public override string MessageTypeIdentifier => "disconnect";

        #endregion

        #region Content

        [DataMember(Name = "reason")]
        public string Reason { get; private set; }

        #endregion

        #region Create

        /// <summary>
        /// Default constructor for serialization.
        /// </summary>
        protected DisconnectMessage() : base() { }

        /// <summary>
        /// Construct by reason.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="reason"></param>
        public DisconnectMessage(String identifier, String reason) : base(identifier)
        {
            this.Reason = reason;
        }

        #endregion

    }
}
