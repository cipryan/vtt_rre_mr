﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Vector.Cloud.Logging.LiveDataChannel.Root.Messages.Data;

namespace Vector.Cloud.Logging.LiveDataChannel.Root.Messages.Logger
{
    public class PulseMessage : ALoggerLdcMessage
    {

        #region Publish

        /// <summary>
        /// Message type identifier.
        /// </summary>
        [IgnoreDataMember]
        public override string MessageTypeIdentifier => "pulse";

        #endregion

        #region Content

        /// <summary>
        /// GPS info.
        /// </summary>
        [DataMember(Name = "gps")]
        public Gps Gps { get; private set; }

        #endregion

        #region Create

        /// <summary>
        /// Default constructor for serialization.
        /// </summary>
        protected PulseMessage() : base() { }

        /// <summary>
        /// Constrcut by values.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="altitude"></param>
        /// <param name="speedMs"></param>
        /// <param name="courseDegree"></param>
        public PulseMessage(String identifier, double latitude, double longitude, double altitude, double speedMs, double courseDegree)
            : this(identifier, new Gps(latitude, longitude, altitude, speedMs, courseDegree))
        { }

        /// <summary>
        /// Construct by reason.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="gps"></param>
        public PulseMessage(String identifier, Gps gps) : base(identifier)
        {
            this.Gps = Gps;
        }

        #endregion

    }
}
