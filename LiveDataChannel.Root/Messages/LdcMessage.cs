﻿using System;
using System.Runtime.Serialization;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish;

namespace Vector.Cloud.Logging.LiveDataChannel.Root.Messages
{

    /// <summary>
    /// Live-Data-Channel message.
    /// </summary>
    [DataContract]
    public abstract class LdcMessage : LiveDataPublishMessageJson
    {

        #region Publish

        /// <summary>
        /// Role type identifier.
        /// </summary>
        [IgnoreDataMember]
        public abstract string RoleTypeIdentifier { get; }

        /// <summary>
        /// Message type identifier.
        /// </summary>
        [IgnoreDataMember]
        public abstract string MessageTypeIdentifier { get; }

        /// <summary>
        /// Entity identifier.
        /// </summary>
        [IgnoreDataMember]
        public readonly String EntityIdentifier;

        /// <summary>
        /// Publish topic identifier.
        /// </summary>
        [IgnoreDataMember]
        public override string PublishTopic => String.Format("logging/ldc/{0}/{1}/{2}",
            this.RoleTypeIdentifier, this.EntityIdentifier, this.MessageTypeIdentifier);

        #endregion

        #region Create

        /// <summary>
        /// Default constructor for serialization.
        /// </summary>
        protected LdcMessage() : base() { }

        /// <summary>
        /// Construct with identifier.
        /// </summary>
        /// <param name="identifier"></param>
        public LdcMessage(String identifier) : this()
        {
            this.EntityIdentifier = identifier;
        }

        #endregion

    }
}
