﻿using System;
using MQTTnet;
using MQTTnet.Client.Publishing;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish
{

    internal class LiveDataPublishException : Exception
    {
        public MqttClientPublishResult MqttClientPublishResult { get; private set; }

        public MqttApplicationMessage MqttApplicationMessage { get; private set; }

        public LiveDataPublishException(MqttApplicationMessage applicationMessage, MqttClientPublishResult result) : base(GetMessage(applicationMessage, result))
        {
            this.MqttApplicationMessage = applicationMessage;
            this.MqttClientPublishResult = result;
        }

        private static string GetMessage(MqttApplicationMessage applicationMessage, MqttClientPublishResult result)
        {
            return string.Format("Live-Data-MQTT message({0}) with packetId '{1}' failed to publish: {2} / {3}", applicationMessage.Topic, result.PacketIdentifier, result.ReasonCode, result.ReasonString);
        }
    }
}