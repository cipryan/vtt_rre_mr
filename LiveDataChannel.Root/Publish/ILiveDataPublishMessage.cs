﻿using MQTTnet;
using MQTTnet.Protocol;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish
{
    public interface ILiveDataPublishMessage
    {
        MqttQualityOfServiceLevel PublishMqttQosLevel { get; }

        string PublishTopic { get; }

        MqttApplicationMessage GetMqttApplicationMessage();
    }
}