﻿using MQTTnet;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish
{

    /// <summary>
    /// Live data byte array publish message.
    /// </summary>
    public class LiveDataPublishMessageBytes : LiveDataPublishMessage
    {

        public override string PublishTopic => this.TopicString;

        public readonly string TopicString;

        public readonly byte[] Bytes;

        /// <summary>
        /// Construct by string.
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="payload"></param>
        public LiveDataPublishMessageBytes(string topic, byte[] bytes) : base()
        {
            this.TopicString = topic;
            this.Bytes = bytes;
        }

        /// <summary>
        /// Set payload.
        /// </summary>
        /// <param name="mamb"></param>
        protected override void SetPayload(MqttApplicationMessageBuilder mamb)
        {
            mamb.WithPayload(this.Bytes);
        }

    }
}
