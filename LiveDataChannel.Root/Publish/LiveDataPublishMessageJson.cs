﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using MQTTnet;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish
{

    /// <summary>
    /// Live data JSON publish message.
    /// </summary>
    [DataContract]
    public abstract class LiveDataPublishMessageJson : LiveDataPublishMessage
    {

        /// <summary>
        /// Set payload.
        /// </summary>
        /// <param name="mamb"></param>
        protected override void SetPayload(MqttApplicationMessageBuilder mamb)
        {
            string jsonString = "";
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(this.GetType());
            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, this);
                jsonString = Encoding.UTF8.GetString(ms.ToArray());
            }
            mamb.WithPayload(jsonString);
            mamb.WithPayloadFormatIndicator(MQTTnet.Protocol.MqttPayloadFormatIndicator.CharacterData);
        }

    }
}
