﻿using System.Runtime.Serialization;
using MQTTnet;
using MQTTnet.Protocol;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish
{

    /// <summary>
    /// Live data publish message.
    /// </summary>
    [DataContract]
    public abstract class LiveDataPublishMessage : ILiveDataPublishMessage
    {

        #region MQTT

        /// <summary>
        /// Topic.
        /// </summary>
        [IgnoreDataMember]
        public abstract string PublishTopic { get; }

        /// <summary>
        /// Quality of service level between broker and this (server) client.
        /// </summary>
        [IgnoreDataMember]
        public virtual MqttQualityOfServiceLevel PublishMqttQosLevel => MqttQualityOfServiceLevel.AtMostOnce;

        #endregion

        #region Content

        /// <summary>
        /// Generate MqttApplicationMessage.
        /// </summary>
        /// <returns></returns>
        public MqttApplicationMessage GetMqttApplicationMessage()
        {
            var mamb = new MqttApplicationMessageBuilder()
                .WithTopic(this.PublishTopic)
                .WithQualityOfServiceLevel(this.PublishMqttQosLevel);
            this.SetPayload(mamb);
            return mamb.Build();
        }

        /// <summary>
        /// Set payload.
        /// </summary>
        /// <param name="mamb"></param>
        protected abstract void SetPayload(MqttApplicationMessageBuilder mamb);

        #endregion

    }
}