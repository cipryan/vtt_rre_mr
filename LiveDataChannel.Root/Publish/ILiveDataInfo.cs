﻿using MQTTnet.Protocol;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish
{

    /// <summary>
    /// Provides basic live data MQTT message information.
    /// </summary>
    public interface ILiveDataInfo
    {

        /// <summary>
        /// Topic.
        /// </summary>
        string Topic { get; }

        /// <summary>
        /// Quality of service level between broker and this (server) client.
        /// </summary>
        MqttQualityOfServiceLevel MqttQualityOfServiceLevel { get; }
    }
}
