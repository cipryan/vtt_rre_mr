﻿using MQTTnet;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish
{

    /// <summary>
    /// Live data string publish message.
    /// </summary>
    public class LiveDataPublishMessageString : LiveDataPublishMessage
    {

        public override string PublishTopic => this.TopicString;

        public readonly string TopicString;

        public readonly string Payload;

        /// <summary>
        /// Construct by topic and string.
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="payload"></param>
        public LiveDataPublishMessageString(string topic, string payload)
        {
            this.TopicString = topic;
            this.Payload = payload;
        }

        /// <summary>
        /// Set payload.
        /// </summary>
        /// <param name="mamb"></param>
        protected override void SetPayload(MqttApplicationMessageBuilder mamb)
        {
            mamb.WithPayload(this.Payload);
            mamb.WithPayloadFormatIndicator(MQTTnet.Protocol.MqttPayloadFormatIndicator.CharacterData);
        }
    }
}
