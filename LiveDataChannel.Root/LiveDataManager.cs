﻿using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Options;
using MQTTnet.Client.Publishing;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Configuration;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Logging;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Publish;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Subscribe;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root
{


    /// <summary>
    /// Live data channel MQTT manager without defined storage repository type. 
    /// </summary>
    public class LiveDataManager : LiveDataManager<object>
    {

        /// <summary>
        /// Construct.
        /// </summary>
        /// <param name="mqttClientOptions"></param>
        /// <param name="subscriptionMessageHandlers"></param>
        /// <param name="logger"></param>
        /// <param name="liveDataManagerConfiguration"></param>
        public LiveDataManager(
            IMqttClientOptions mqttClientOptions,
            List<ILiveDataSubscriptionHandler<object>> subscriptionMessageHandlers,
            ILogger logger = null,
            ILiveDataManagerConfiguration liveDataManagerConfiguration = null
        ) : base(mqttClientOptions, null, subscriptionMessageHandlers, logger, liveDataManagerConfiguration) { }
    }

    /// <summary>
    /// Live data channel MQTT manager with defined storage repository type.
    /// </summary>
    /// <typeparam name="TStorageRepository"></typeparam>
    public class LiveDataManager<TStorageRepository> where TStorageRepository : class
    {

        #region Properties

        /// <summary>
        /// MQTT client options.
        /// </summary>
        protected readonly IMqttClientOptions MqttClientOptions;

        /// <summary>
        /// Storage repository.
        /// </summary>
        protected readonly TStorageRepository StorageRepository;

        /// <summary>
        /// Subscription message handlers.
        /// </summary>
        protected readonly List<ILiveDataSubscriptionHandler<TStorageRepository>> InternalMessageSubscriptionHandlers;

        /// <summary>
        /// Logger.
        /// </summary>
        protected readonly ILogger Logger;

        /// <summary>
        /// Configuration.
        /// </summary>
        protected readonly ILiveDataManagerConfiguration Configuration;

        #endregion

        #region State

        /// <summary>
        /// Determine if is already initialized.
        /// </summary>
        public bool IsInitialized { get; private set; }

        /// <summary>
        /// Determine if is connected.
        /// </summary>
        public bool IsConnected { get; private set; }

        /// <summary>
        /// Count concurrent disconnects to determine reconnect delay.
        /// </summary>
        public uint ConcurrentDisconnects { get; private set; }

        /// <summary>
        /// MQTT client instance.
        /// </summary>
        protected IMqttClient MqttClient;

        #endregion

        /// <summary>
        /// Get builder.
        /// </summary>
        public static LiveDataManagerBuilder Builder => new LiveDataManagerBuilder();

        /// <summary>
        /// Construct.
        /// </summary>
        /// <param name="mqttClientOptions"></param>
        /// <param name="storageRepository"></param>
        /// <param name="subscriptionMessageHandlers"></param>
        /// <param name="logger"></param>
        /// <param name="liveDataManagerConfiguration"></param>
        public LiveDataManager(
            IMqttClientOptions mqttClientOptions,
            TStorageRepository storageRepository,
            List<ILiveDataSubscriptionHandler<TStorageRepository>> subscriptionMessageHandlers,
            ILogger logger = null,
            ILiveDataManagerConfiguration liveDataManagerConfiguration = null)
        {
            // State
            this.IsInitialized = false;
            this.IsConnected = false;
            this.ConcurrentDisconnects = 0;
            // Store properties
            this.MqttClientOptions = mqttClientOptions ?? throw new ArgumentNullException(nameof(mqttClientOptions));
            this.StorageRepository = storageRepository;
            this.InternalMessageSubscriptionHandlers = subscriptionMessageHandlers ?? throw new ArgumentNullException(nameof(subscriptionMessageHandlers));
            this.Logger = logger ?? DummyLogger.Create();
            this.Configuration = liveDataManagerConfiguration ?? LiveDataManagerConfiguration.Default();
        }

        #region Initialize

        /// <summary>
        /// Initialize synchronously.
        /// </summary>
        public void Init()
        {
            this.InitAsync().GetAwaiter();
        }

        /// <summary>
        /// Initialize async.
        /// </summary>
        /// <returns></returns>
        public async Task InitAsync()
        {

            // Init once only
            if (this.IsInitialized)
            {
                await Task.CompletedTask;
            }

            // Log discovered handlers
            this.Logger.LogInformation("Live-Data-MQTT found {0} handlers:", this.InternalMessageSubscriptionHandlers.Count);
            foreach (ILiveDataSubscriptionHandler<TStorageRepository> handler in this.InternalMessageSubscriptionHandlers)
            {
                this.Logger.LogInformation("               - {0}", handler.GetType().Name);
            }

            // Client
            MqttFactory factory = new MqttFactory();
            this.MqttClient = factory.CreateMqttClient();

            // Connected handler
            this.MqttClient.UseConnectedHandler(async h =>
            {
                this.Logger.LogInformation("Live-Data-MQTT connected");
                this.ConcurrentDisconnects = 0;
                this.IsConnected = true;
                await this.SubscribeTopics();
            });

            // Message handler
            this.MqttClient.UseApplicationMessageReceivedHandler(async h =>
            {
                // Ignore messages directly from server - mark as handled
                if (h.ClientId == null)
                {
                    return;
                }
                // handle
                try
                {
                    bool handled = await this.HandleApplicationMessage(h.ClientId, h.ApplicationMessage);
                    if (!handled)
                    {
                        h.ProcessingFailed = true;
                        this.Logger.LogWarning("Live-Data-MQTT message({0}) was not handled: unknown topic", h.ApplicationMessage.Topic);
                    }
                }
                catch (Exception e)
                {
                    this.Logger.LogWarning("Live-Data-MQTT message({0}) handling failed: {0}", h.ApplicationMessage.Topic, e.Message);
                }
            });

            // Diconnect handler
            this.MqttClient.UseDisconnectedHandler(this.HandleDisconnect);

            // Connect
            this.Logger.LogInformation("Live-Data-MQTT connecting");
            await this.Connect();
        }

        /// <summary>
        /// Try to connect.
        /// </summary>
        /// <returns></returns>
        protected async Task Connect()
        {
            try
            {
                await this.MqttClient.ConnectAsync(this.MqttClientOptions, CancellationToken.None);
            }
            catch (Exception e)
            {
                //await this.HandleDisconnect(new MqttClientDisconnectedEventArgs(false, e, new MqttClientAuthenticateResult()));
            }

        }

        /// <summary>
        /// Handle disconnect.
        /// </summary>
        /// <param name="h"></param>
        /// <returns></returns>
        protected async Task HandleDisconnect(MqttClientDisconnectedEventArgs h)
        {
            this.IsConnected = false;
            int delay = Convert.ToInt32(Math.Min(this.Configuration.ReconnectDelayBase * Math.Pow(2, this.ConcurrentDisconnects),
                this.Configuration.MaximumReconnectDelay));
            this.ConcurrentDisconnects++;
            String message = "unknown";
            if (h.Exception != null)
            {
                message = h.Exception.Message;
            }
            this.Logger.LogInformation("Live-Data disconnected: {0}", message);
            this.Logger.LogDebug("Live-Data waiting for reconnection-attempt in {0}s", (delay / 1000.0d));
            await Task.Delay(delay);
            this.Logger.LogDebug("Mqtt-Serializable-Data reconnecting");
            try
            {
                await this.Connect();
            }
            catch (Exception e)
            {
                this.Logger.LogDebug("Mqtt-Serializable-Data reconnect failed: {0}", e.Message);
            }
        }

        #endregion

        #region Subscribe

        /// <summary>
        /// Subscribe to required topics.
        /// </summary>
        /// <returns></returns>
        protected async Task SubscribeTopics()
        {
            this.Logger.LogInformation("Live-Data-MQTT Subscribing to {0} topics:", this.InternalMessageSubscriptionHandlers.Count);
            foreach (ILiveDataSubscriptionHandler<TStorageRepository> messageSubscriptionHandler in this.InternalMessageSubscriptionHandlers)
            {
                TopicFilter topicFilter = new TopicFilterBuilder()
                    .WithQualityOfServiceLevel(messageSubscriptionHandler.MqttQualityOfServiceLevel)
                    .WithTopic(messageSubscriptionHandler.Topic)
                    .Build();
                this.Logger.LogInformation("               - {0}", topicFilter.Topic);
                await this.MqttClient.SubscribeAsync(topicFilter);
            }
        }

        /// <summary>
        /// List of registered messages.
        /// </summary>
        public IReadOnlyList<ILiveDataSubscriptionHandler<TStorageRepository>> MessageSubscriptionHandlers
        {
            get
            {
                return this.InternalMessageSubscriptionHandlers.AsReadOnly();
            }
        }

        /// <summary>
        /// Handle message.
        /// </summary>
        /// <param name="namespaceString"></param>
        /// <param name="payload"></param>
        /// <returns>Determine if message was handled</returns>
        protected async Task<bool> HandleApplicationMessage(string clientId, MqttApplicationMessage applicationMessage)
        {
            // Check if topic matches any known message
            bool handled = false;
            foreach (ILiveDataSubscriptionHandler<TStorageRepository> messageSubscriptionHandler in this.InternalMessageSubscriptionHandlers)
            {
                if (!messageSubscriptionHandler.TopicPatternMatches(applicationMessage.Topic))
                {
                    continue;
                }
                bool continueHandling = await messageSubscriptionHandler.HandleApplicationMessage(this, applicationMessage, this.Logger, this.StorageRepository);
                handled = true;
                if (!continueHandling)
                {
                    break;
                }
            }
            // return
            return await Task.FromResult<bool>(handled);
        }

        #endregion

        #region Publish

        /// <summary>
        /// Publish message.
        /// </summary>
        /// <param name="liveDataPublishMessage"></param>
        /// <returns></returns>
        public async Task PublishMessageAsync(LiveDataPublishMessage liveDataPublishMessage)
        {
            MqttApplicationMessage mqttApplicationMessage = liveDataPublishMessage.GetMqttApplicationMessage();
            this.Logger.LogInformation("Live-Data-MQTT Publishing message '{0}'", liveDataPublishMessage.GetType().Name);
            MqttClientPublishResult result = await this.MqttClient.PublishAsync(mqttApplicationMessage);
            if (result.ReasonCode.Equals(MqttClientPublishReasonCode.Success))
            {
                return;
            }
            this.Logger.LogError("Live-Data-MQTT Publishing message '{0}' failed: {1}",
                liveDataPublishMessage.GetType().Name,
                result.ReasonString);
            throw new LiveDataPublishException(mqttApplicationMessage, result);
        }

        /// <summary>
        /// Publish message.
        /// </summary>
        /// <param name="liveDataPublishMessage"></param>
        /// <returns></returns>
        public void PublishMessage(LiveDataPublishMessage liveDataPublishMessage)
        {
            this.PublishMessageAsync(liveDataPublishMessage).GetAwaiter();
        }

        #endregion

    }
}
