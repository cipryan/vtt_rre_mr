﻿namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Configuration
{

    /// <summary>
    /// Live data mangager configuration.
    /// </summary>
    public interface ILiveDataManagerConfiguration
    {

        /// <summary>
        /// Reconnect delay. Will be doubled with each concurrent reconnect failure up to specified maximum reconnect delay.
        /// </summary>
        double ReconnectDelayBase { get; }

        /// <summary>
        /// Maximum delay between reconnect tries.
        /// </summary>
        double MaximumReconnectDelay { get; }

    }
}
