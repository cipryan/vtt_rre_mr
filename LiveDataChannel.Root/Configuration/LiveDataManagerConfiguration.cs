﻿namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Configuration
{

    /// <summary>
    /// Live data manger configuration.
    /// </summary>
    public class LiveDataManagerConfiguration : ILiveDataManagerConfiguration
    {

        /// <summary>
        /// Reconnect delay. Will be doubled with each concurrent reconnect failure up to specified maximum reconnect delay.
        /// </summary>
        public double ReconnectDelayBase { get; private set; }

        /// <summary>
        /// Maximum delay between reconnect tries.
        /// </summary>
        public double MaximumReconnectDelay { get; private set; }

        /// <summary>
        /// Construct readonly configuration.
        /// </summary>
        /// <param name="reconnectDelayBase"></param>
        /// <param name="maximumReconnectDelay"></param>
        public LiveDataManagerConfiguration(double reconnectDelayBase, double maximumReconnectDelay)
        {
            this.ReconnectDelayBase = reconnectDelayBase;
            this.MaximumReconnectDelay = maximumReconnectDelay;
        }

        /// <summary>
        /// Get default configuration.
        /// </summary>
        /// <returns></returns>
        public static LiveDataManagerConfiguration Default()
        {
            return new LiveDataManagerConfiguration(
                1000, // 1 second
                (30 * 60 * 1000) // 30 minutes
            );
        }

    }
}
