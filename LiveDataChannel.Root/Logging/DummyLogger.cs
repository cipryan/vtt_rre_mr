﻿using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Logging;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Logging
{

    /// <summary>
    /// Dummy implementation of ILogger.
    /// </summary>
    public class DummyLogger : ILogger
    {

        /// <summary>
        /// Static builder.
        /// </summary>
        /// <returns></returns>
        public static DummyLogger Create()
        {
            return new DummyLogger();
        }

        public void LogDebug(string message, params object[] args)
        {
            // do nothing
        }

        public void LogError(string message, params object[] args)
        {
            // do nothing
        }

        public void LogInformation(string message, params object[] args)
        {
            // do nothing
        }

        public void LogWarning(string message, params object[] args)
        {
            // do nothing
        }
    }
}
