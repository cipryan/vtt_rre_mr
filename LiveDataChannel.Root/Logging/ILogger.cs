﻿namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Logging
{
    public interface ILogger
    {
        void LogError(string message, params object[] args);
        void LogInformation(string message, params object[] args);
        void LogDebug(string message, params object[] args);
        void LogWarning(string message, params object[] args);
    }
}