﻿using System;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Logging;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Logging
{

    /// <summary>
    /// Console implementation of ILogger.
    /// </summary>
    public class ConsoleLogger : ILogger
    {

        /// <summary>
        /// Static builder.
        /// </summary>
        /// <returns></returns>
        public static ConsoleLogger Create()
        {
            return new ConsoleLogger();
        }

        public void LogDebug(string message, params object[] args)
        {
            Console.WriteLine(string.Format("DEBUG: " + message, args));
        }

        public void LogError(string message, params object[] args)
        {
            Console.WriteLine(string.Format("ERROR: " + message, args));
        }

        public void LogInformation(string message, params object[] args)
        {
            Console.WriteLine(string.Format("INFO: " + message, args));
        }

        public void LogWarning(string message, params object[] args)
        {
            Console.WriteLine(string.Format("WARN: " + message, args));
        }
    }
}
