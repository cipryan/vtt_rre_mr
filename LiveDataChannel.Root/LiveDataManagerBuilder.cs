﻿using MQTTnet.Client.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Configuration;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Logging;
using Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root.Subscribe;

namespace Vector.Cloud.Logging.ControlRoom.LiveDataChannel.Root
{

    /// <summary>
    /// Builder for LiveDataManager (define mqtt options).
    /// </summary>
    public class LiveDataManagerBuilder
    {

        /// <summary>
        /// Construct empty.
        /// </summary>
        public LiveDataManagerBuilder() { }

        /// <summary>
        /// Add IMqttClientOptions.
        /// </summary>
        /// <param name="mqttClientOptions"></param>
        /// <returns></returns>
        public StorageLiveDataManagerBuilder WithMqttClientOptions(IMqttClientOptions mqttClientOptions)
        {
            if (mqttClientOptions == null)
            {
                throw new ArgumentNullException(nameof(mqttClientOptions));
            }
            return new StorageLiveDataManagerBuilder(mqttClientOptions);
        }
    }

    /// <summary>
    /// Builder for LiveDataManager (define storage).
    /// </summary>
    public class StorageLiveDataManagerBuilder
    {

        #region Properties

        /// <summary>
        /// MQTT client options.
        /// </summary>
        protected readonly IMqttClientOptions MqttClientOptions;

        #endregion

        /// <summary>
        /// Construct by IMqttClientOptions.
        /// </summary>
        /// <param name="mqttClientOptions"></param>
        public StorageLiveDataManagerBuilder(IMqttClientOptions mqttClientOptions)
        {
            this.MqttClientOptions = mqttClientOptions;
        }

        #region StorageRepository

        /// <summary>
        /// Set no sorage repository.
        /// </summary>
        /// <returns></returns>
        public SubscriptionMessageLiveDataManagerBuilder WithoutStorageRepository()
        {
            return new SubscriptionMessageLiveDataManagerBuilder(this.MqttClientOptions);
        }

        /// <summary>
        /// Add sorage repository.
        /// </summary>
        /// <typeparam name="TStorageRepository"></typeparam>
        /// <param name="storageRepository"></param>
        /// <returns></returns>
        public SubscriptionMessageLiveDataManagerBuilder<TStorageRepository> WithStorageRepository<TStorageRepository>(TStorageRepository storageRepository)
            where TStorageRepository : class
        {
            if (storageRepository == null)
            {
                throw new ArgumentNullException(nameof(storageRepository));
            }
            return new SubscriptionMessageLiveDataManagerBuilder<TStorageRepository>(this.MqttClientOptions, storageRepository);
        }

        #endregion

    }

    /// <summary>
    /// Builder for LiveDataManager (define subscriptions).
    /// </summary>
    public class SubscriptionMessageLiveDataManagerBuilder : SubscriptionMessageLiveDataManagerBuilder<object>
    {

        /// <summary>
        /// Create by mqttClientOptions.
        /// </summary>
        /// <param name="mqttClientOptions"></param>
        internal SubscriptionMessageLiveDataManagerBuilder(IMqttClientOptions mqttClientOptions) : base(mqttClientOptions) { }

        #region Subscription

        /// <summary>
        /// Find all viable subscription message handlers in assembly that extend specified type parameter.
        /// </summary>
        /// <param name="namespaceString"></param>
        /// <param name="assemblies"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public new BuildableLiveDataManagerBuilder WithReflectedSubscriptionMessageHandlers<T>(String namespaceString = null, List<Assembly> assemblies = null)
            where T : ILiveDataSubscriptionHandler<object>
        {
            List<ILiveDataSubscriptionHandler<object>> handlers = this.DiscoverMqttMessageHandlers<T>(namespaceString, assemblies);
            return this.WithSpecificSubscriptionMessageHandlers(handlers);
        }

        /// <summary>
        /// Find all viable subscription message handlers in assembly
        /// </summary>
        /// <param name="namespaceString"></param>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        public new BuildableLiveDataManagerBuilder WithReflectedSubscriptionMessageHandlers(String namespaceString = null, List<Assembly> assemblies = null)
        {
            return this.WithReflectedSubscriptionMessageHandlers<ILiveDataSubscriptionHandler<object>>(namespaceString, assemblies);
        }

        /// <summary>
        /// Use specified message handlers.
        /// </summary>
        /// <param name="subscriptionMessageHandlers"></param>
        /// <returns></returns>
        public new BuildableLiveDataManagerBuilder WithSpecificSubscriptionMessageHandlers(
            List<ILiveDataSubscriptionHandler<object>> subscriptionMessageHandlers)
        {
            return new BuildableLiveDataManagerBuilder(
                this.MqttClientOptions,
                subscriptionMessageHandlers);
        }

        /// <summary>
        /// Use specified message handlers.
        /// </summary>
        /// <param name="subscriptionMessageHandlers"></param>
        /// <returns></returns>
        public new BuildableLiveDataManagerBuilder WithSpecificSubscriptionMessageHandlers(
        params ILiveDataSubscriptionHandler<object>[] subscriptionMessageHandlers)
        {
            return this.WithSpecificSubscriptionMessageHandlers(subscriptionMessageHandlers.ToList());
        }

        #endregion

    }

    /// <summary>
    /// Builder for LiveDataManager (define subscription).
    /// </summary>
    /// <typeparam name="TStorageRepository"></typeparam>
    public class SubscriptionMessageLiveDataManagerBuilder<TStorageRepository> where TStorageRepository : class
    {

        #region Properties

        /// <summary>
        /// MQTT client options.
        /// </summary>
        protected readonly IMqttClientOptions MqttClientOptions;

        /// <summary>
        /// Storage repository.
        /// </summary>
        protected TStorageRepository StorageRepository;

        #endregion

        /// <summary>
        /// Create by mqttClientOptions and storageRepository.
        /// </summary>
        /// <param name="mqttClientOptions"></param>
        /// <param name="storageRepository"></param>
        internal SubscriptionMessageLiveDataManagerBuilder(IMqttClientOptions mqttClientOptions, TStorageRepository storageRepository = null)
        {
            this.MqttClientOptions = mqttClientOptions;
            this.StorageRepository = storageRepository;
        }

        #region Subscription

        /// <summary>
        /// Find all viable subscription message handlers in assembly that extend specified type parameter.
        /// </summary>
        /// <param name="namespaceString"></param>
        /// <param name="assemblies"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public BuildableLiveDataManagerBuilder<TStorageRepository> WithReflectedSubscriptionMessageHandlers<T>(
            String namespaceString = null, List<Assembly> assemblies = null)
            where T : ILiveDataSubscriptionHandler<TStorageRepository>
        {
            List<ILiveDataSubscriptionHandler<TStorageRepository>> handlers = this.DiscoverMqttMessageHandlers<T>(namespaceString, assemblies);
            return this.WithSpecificSubscriptionMessageHandlers(handlers);
        }

        /// <summary>
        /// Find all viable subscription message handlers in assembly
        /// </summary>
        /// <param name="namespaceString"></param>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        public BuildableLiveDataManagerBuilder<TStorageRepository> WithReflectedSubscriptionMessageHandlers(
            String namespaceString = null, List<Assembly> assemblies = null)
        {
            return this.WithReflectedSubscriptionMessageHandlers<ILiveDataSubscriptionHandler<TStorageRepository>>(namespaceString, assemblies);
        }

        /// <summary>
        /// Use specified message handlers.
        /// </summary>
        /// <param name="subscriptionMessageHandlers"></param>
        /// <returns></returns>
        public BuildableLiveDataManagerBuilder<TStorageRepository> WithSpecificSubscriptionMessageHandlers(
            List<ILiveDataSubscriptionHandler<TStorageRepository>> subscriptionMessageHandlers)
        {
            return new BuildableLiveDataManagerBuilder<TStorageRepository>(
                this.MqttClientOptions,
                this.StorageRepository,
                subscriptionMessageHandlers);
        }

        /// <summary>
        /// Use specified message handlers.
        /// </summary>
        /// <param name="subscriptionMessageHandlers"></param>
        /// <returns></returns>
        public BuildableLiveDataManagerBuilder<TStorageRepository> WithSpecificSubscriptionMessageHandlers(
        params ILiveDataSubscriptionHandler<TStorageRepository>[] subscriptionMessageHandlers)
        {
            return this.WithSpecificSubscriptionMessageHandlers(subscriptionMessageHandlers.ToList());
        }

        /// <summary>
        /// Discover message handlers by reflection.
        /// </summary>
        /// <param name="namespaceString"></param>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        protected List<ILiveDataSubscriptionHandler<TStorageRepository>> DiscoverMqttMessageHandlers(
            String namespaceString = null, List<Assembly> assemblies = null)
        {
            return this.DiscoverMqttMessageHandlers<ILiveDataSubscriptionHandler<TStorageRepository>>(namespaceString, assemblies);
        }

        /// <summary>
        /// Discover message handlers by reflection.
        /// </summary>
        /// <param name="namespaceString"></param>
        /// <param name="assemblies"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected List<ILiveDataSubscriptionHandler<TStorageRepository>> DiscoverMqttMessageHandlers<T>(
            String namespaceString = null, List<Assembly> assemblies = null)
        where T : ILiveDataSubscriptionHandler<TStorageRepository>
        {
            // Return list
            List<ILiveDataSubscriptionHandler<TStorageRepository>> handlers = new List<ILiveDataSubscriptionHandler<TStorageRepository>>();

            // Assemblies
            assemblies = assemblies ?? new List<Assembly>();
            if (assemblies.Count == 0)
            {
                assemblies.Add(Assembly.GetExecutingAssembly());
            }
            
            // Loop
            foreach(Assembly assembly in assemblies)
            {
                // Add messages handlers by reflection
                assembly.GetTypes().Where(t =>
                ((!t.IsAbstract) &&
                    (t.IsClass) &&
                    (!t.IsGenericType) &&
                    (this.IsSubclassOfRawGeneric(typeof(T), t))
                ))
                .ToList()
                .ForEach(liveDataSubscriptionMessageHandlerType =>
                {
                    if (String.IsNullOrEmpty(namespaceString) || (liveDataSubscriptionMessageHandlerType.Namespace.StartsWith(namespaceString)))
                    {
                        ILiveDataSubscriptionHandler<TStorageRepository> handler = (ILiveDataSubscriptionHandler<TStorageRepository>)Activator.CreateInstance(liveDataSubscriptionMessageHandlerType);
                        handlers.Add(handler);
                    }
                });
            }

            // Return
            return handlers;
        }

        /// <summary>
        /// Determine if type is subclass of generic type.
        /// </summary>
        /// <param name="generic"></param>
        /// <param name="toCheck"></param>
        /// <returns></returns>
        public bool IsSubclassOfRawGeneric(Type generic, Type toCheck)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                Type cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                {
                    return true;
                }
                toCheck = toCheck.BaseType;
            }
            return false;
        }


        #endregion

    }

    /// <summary>
    /// Builder for LiveDataManager (define optionals).
    /// </summary>
    public class BuildableLiveDataManagerBuilder : BuildableLiveDataManagerBuilder<object>
    {

        /// <summary>
        /// Create by mqttClientOptions.
        /// </summary>
        /// <param name="mqttClientOptions"></param>
        /// <param name="subscriptionMessageHandlers"></param>
        internal BuildableLiveDataManagerBuilder(
            IMqttClientOptions mqttClientOptions,
            List<ILiveDataSubscriptionHandler<object>> subscriptionMessageHandlers)
            : base(mqttClientOptions, null, subscriptionMessageHandlers) { }

        #region Build

        /// <summary>
        /// Build.
        /// </summary>
        /// <returns></returns>
        public new LiveDataManager Build()
        {
            return new LiveDataManager(this.MqttClientOptions, this.SubscriptionMessageHandlers, this.Logger, this.Configuration);
        }

        #endregion

        #region Optionals

        /// <summary>
        /// Add ILogger;
        /// </summary>
        /// <param name="logger"></param>
        /// <returns></returns>
        public new BuildableLiveDataManagerBuilder WithLogger(ILogger logger)
        {
            base.WithLogger(logger);
            return this;
        }

        /// <summary>
        /// Add configuration.
        /// </summary>
        /// <param name="liveDataManagerConfiguration"></param>
        /// <returns></returns>
        public new BuildableLiveDataManagerBuilder WithConfiguration(ILiveDataManagerConfiguration liveDataManagerConfiguration)
        {
            base.WithConfiguration(liveDataManagerConfiguration);
            return this;
        }

        #endregion

    }

    /// <summary>
    /// Builder for LiveDataManager (define optionals).
    /// </summary>
    /// <typeparam name="TStorageRepository"></typeparam>
    public class BuildableLiveDataManagerBuilder<TStorageRepository> where TStorageRepository : class
    {

        #region Properties

        /// <summary>
        /// MQTT client options.
        /// </summary>
        protected readonly IMqttClientOptions MqttClientOptions;

        /// <summary>
        /// Subscription message handlers.
        /// </summary>
        protected readonly List<ILiveDataSubscriptionHandler<TStorageRepository>> SubscriptionMessageHandlers;

        /// <summary>
        /// Storage repository.
        /// </summary>
        protected readonly TStorageRepository StorageRepository;

        /// <summary>
        /// Logger.
        /// </summary>
        protected ILogger Logger;

        /// <summary>
        /// Configuration.
        /// </summary>
        protected ILiveDataManagerConfiguration Configuration;

        #endregion

        /// <summary>
        /// Create by mqttClientOptions and storageRepository.
        /// </summary>
        /// <param name="mqttClientOptions"></param>
        /// <param name="storageRepository"></param>
        /// <param name="subscriptionMessageHandlers"></param>
        internal BuildableLiveDataManagerBuilder(IMqttClientOptions mqttClientOptions, TStorageRepository storageRepository,
            List<ILiveDataSubscriptionHandler<TStorageRepository>> subscriptionMessageHandlers)
        {
            this.MqttClientOptions = mqttClientOptions;
            this.StorageRepository = storageRepository;
            this.SubscriptionMessageHandlers = subscriptionMessageHandlers;
        }

        #region Optionals

        /// <summary>
        /// Add ILogger;
        /// </summary>
        /// <param name="logger"></param>
        /// <returns></returns>
        public BuildableLiveDataManagerBuilder<TStorageRepository> WithLogger(ILogger logger)
        {
            this.Logger = logger;
            return this;
        }

        /// <summary>
        /// Add configuration.
        /// </summary>
        /// <param name="liveDataManagerConfiguration"></param>
        /// <returns></returns>
        public BuildableLiveDataManagerBuilder<TStorageRepository> WithConfiguration(ILiveDataManagerConfiguration liveDataManagerConfiguration)
        {
            this.Configuration = liveDataManagerConfiguration;
            return this;
        }

        #endregion

        #region Build

        /// <summary>
        /// Build.
        /// </summary>
        /// <returns></returns>
        public LiveDataManager<TStorageRepository> Build()
        {
            return new LiveDataManager<TStorageRepository>(
                this.MqttClientOptions,
                this.StorageRepository,
                this.SubscriptionMessageHandlers,
                this.Logger,
                this.Configuration);
        }

        #endregion
    }
}